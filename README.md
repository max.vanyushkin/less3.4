Развернуто 2 машины
  - 34.65.30.244
  - 34.65.197.131

Как протестировать 


	curl --header 'Host: test1.juneway.pro' http://34.65.30.244;
	curl --header 'Host: test1.juneway.pro' http://34.65.197.131;
	curl --header 'Host: test2.juneway.pro' http://34.65.30.244;
	curl --header 'Host: test2.juneway.pro' http://34.65.197.131;

Что лежит на первой машине

	root@less-3-4node-2:/etc/nginx/conf.d# ls -l
	total 8
	--w----r-T 1 root root 189 Jan 13 17:56 upstream_test1.juneway.pro.conf
	--w----r-T 1 root root 189 Jan 13 17:56 upstream_test2.juneway.pro.conf
	root@less-3-4node-2:/etc/nginx/conf.d# cat upstream_test1.juneway.pro.conf 
		upstream test1.juneway.pro {
						server 127.0.0.1:8002 ; 
						server 127.0.0.1:8003 ; 
						server 127.0.0.2:8004 ; 
				}   
		root@less-3-4node-2:/etc/nginx/conf.d# cat upstream_test2.juneway.pro.conf 
		upstream test2.juneway.pro {
						server 127.0.0.1:8002 ; 
						server 127.0.0.1:8003 ; 
						server 127.0.0.2:8004 ; 
				}   
		root@less-3-4node-2:/etc/nginx/conf.d# 

Что лежит на второй машине

	root@less-3-4node-1:/etc/nginx/conf.d# ls -l
	total 8
	--w----r-T 1 root root 189 Jan 13 17:56 upstream_test1.juneway.pro.conf
	--w----r-T 1 root root 189 Jan 13 17:56 upstream_test2.juneway.pro.conf
	root@less-3-4node-1:/etc/nginx/conf.d# cat upstream_test1.juneway.pro.conf 
		upstream test1.juneway.pro {
						server 127.0.0.1:8002 ; 
						server 127.0.0.1:8003 ; 
						server 127.0.0.2:8004 ; 
				}   
		root@less-3-4node-1:/etc/nginx/conf.d# cat upstream_test2.juneway.pro.conf 
		upstream test2.juneway.pro {
						server 127.0.0.1:8002 ; 
						server 127.0.0.1:8003 ; 
						server 127.0.0.2:8004 ; 
				}   
		root@less-3-4node-1:/etc/nginx/conf.d# 
